import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PostService } from '../post.service';
import { Observable } from 'rxjs';
import { PostI } from '../../../shared/models/post.interface';
import { AngularEditorConfig } from '@kolkov/angular-editor';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-details-post',
  templateUrl: './details-post.component.html',
  styleUrls: ['./details-post.component.scss']
})
export class DetailsPostComponent implements OnInit {
  public post$: Observable<PostI>;
  public posts$: Observable<PostI[]>;
  public listaPosts = [];
  public mostrar = false;
  htmlContent = '';

  config: AngularEditorConfig = {
    editable: true,
    spellcheck: true,
    height: '100%',
    minHeight: '5rem',
    placeholder: 'Enter text here...',
    translate: 'no',
    defaultParagraphSeparator: 'p',
    defaultFontName: 'Arial',
    toolbarHiddenButtons: [
      ['bold']
      ],
    customClasses: [
      {
        name: "quote",
        class: "quote",
      },
      {
        name: 'redText',
        class: 'redText'
      },
      {
        name: "titleText",
        class: "titleText",
        tag: "h1",
      },
    ]
  };
  constructor(private route: ActivatedRoute, private postSvc: PostService) { }
  public editPostForm = new FormGroup({
    contentPost: new FormControl('', Validators.required),
  });
  ngOnInit() {
    const idPost = this.route.snapshot.params.id;
    this.posts$ = this.postSvc.getAllPostsId(idPost);
    this.posts$.subscribe((items)=> {
      for(let i =0; i < items.length;i++){
        if(items[i]!== undefined) {
          this.listaPosts.push(items[i]);
        }
      }
      if(this.listaPosts.length>0){
        this.mostrar = true;
      }
    });
  }

  buscarPost(id: any) {
    const idPost = id;
    this.post$ = this.postSvc.getOnePost(idPost);
    this.post$.subscribe((items)=> {
      this.htmlContent = items['contentPost'];
      this.initValuesForm(this.htmlContent);
    });
  }

  initValuesForm(items): void {
    this.mostrar = false;
    this.editPostForm.patchValue({
      contentPost: items,
    });
  }

}
