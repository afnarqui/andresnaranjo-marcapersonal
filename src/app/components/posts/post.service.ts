import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { map, finalize } from 'rxjs/operators';
import { PostI } from '../../shared/models/post.interface';
import { FileI } from '../../shared/models/file.interface';
import { AngularFireStorage } from '@angular/fire/storage';
import { CursoI } from 'src/app/shared/models/curso.interface';

@Injectable({
  providedIn: 'root'
})
export class PostService {
  private postsCollection: AngularFirestoreCollection<PostI>;
  private cursoCollection: AngularFirestoreCollection<CursoI>;
  private filePath: any;
  private downloadURL: Observable<string>;

  constructor(
    private afs: AngularFirestore,
    private storage: AngularFireStorage
  ) {
    this.postsCollection = afs.collection<PostI>('post', ref => ref.orderBy('fecha'));
    this.cursoCollection = afs.collection<CursoI>('curso', ref => ref.orderBy('fecha'));
  }

  public getAllPosts(): Observable<PostI[]> {
    return this.postsCollection
      .snapshotChanges()
      .pipe(
        map(actions =>
          actions.map(a => {
            const data = a.payload.doc.data() as PostI;
            const id = a.payload.doc.id;
            return { id, ...data };
          })
        )
      );
  }

  public getAllPostsId(id: any): Observable<PostI[]> {
    let nuevoId = id;
    return this.postsCollection
      .snapshotChanges()
      .pipe(
        map(actions =>
          actions.map(a => {
            const data = a.payload.doc.data() as PostI;
            const id = a.payload.doc.id;
            if(data.cursosId == nuevoId){
              return { id, ...data };
            }
            // return { id, ...data };
          })
        )
      );
  }

  public buscarCursos(): Observable<CursoI[]> {
    return this.cursoCollection
      .snapshotChanges()
      .pipe(
        map(actions =>
          actions.map(a => {
            const data = a.payload.doc.data() as CursoI;
            const id = a.payload.doc.id;
            return { id, ...data };
          })
        )
      );
  }

  public getOnePost(id: PostI): Observable<PostI> {
    return this.afs.doc<PostI>(`post/${id}`).valueChanges();
  }

  public deletePostById(post: PostI) {
    return this.postsCollection.doc(post.id).delete();
  }

  public editPostById(post: PostI, newImage?: FileI) {
    if (newImage) {
      this.uploadImage(post, newImage);
    } else {
      const fechaActual =  new Date();
      const nuevaFecha = new Date(fechaActual)
      post.fecha = nuevaFecha;
      return this.postsCollection.doc(post.id).update(post);
    }
  }

  public preAddAndUpdatePost(post: PostI, image: FileI): void {
    this.uploadImage(post, image);
  }

  public preAddAndUpdateCurso(curso: CursoI, image: FileI): void {
    this.uploadImagenCurso(curso, image);
  }

  private uploadImagenCurso(curso: CursoI, image: FileI) {
    this.filePath = `images/${image.name}`;
    const fileRef = this.storage.ref(this.filePath);
    const task = this.storage.upload(this.filePath, image);
    task.snapshotChanges()
      .pipe(
        finalize(() => {
          fileRef.getDownloadURL().subscribe(urlImage => {
            this.downloadURL = urlImage;
            this.guardarCurso(curso);
          });
        })
      ).subscribe();
  }

  private savePost(post: PostI) {
    const fechaActual =  new Date();
    const nuevaFecha = new Date(fechaActual)
    const postObj: PostI = {
      titlePost: post.titlePost,
      contentPost: post.contentPost,
      imagePost: this.downloadURL,
      fileRef: this.filePath,
      fecha: nuevaFecha,
      cursosId: post.cursosId,
    };
    if (post.id) {
      postObj.fecha = post.fecha === undefined ? postObj.fecha : post.fecha;
      return this.postsCollection.doc(post.id).update(postObj);
    } else {
      return this.postsCollection.add(postObj);
    }

  }

  private guardarCurso(curso: CursoI) {
    const fechaActual =  new Date();
    const nuevaFecha = new Date(fechaActual)
    const cursoObj: CursoI = {
      nombre: curso.nombre,
      imagen: this.downloadURL,
      fileRef: this.filePath,
      fecha: nuevaFecha
    };
    if (curso.id) {
      cursoObj.fecha = curso.fecha;
      return this.cursoCollection.doc(curso.id).update(cursoObj);
    } else {
      return this.cursoCollection.add(cursoObj);
    }
 }

  private uploadImage(post: PostI, image: FileI) {
    this.filePath = `images/${image.name}`;
    const fileRef = this.storage.ref(this.filePath);
    const task = this.storage.upload(this.filePath, image);
    task.snapshotChanges()
      .pipe(
        finalize(() => {
          fileRef.getDownloadURL().subscribe(urlImage => {
            this.downloadURL = urlImage;
            this.savePost(post);
          });
        })
      ).subscribe();
  }
}
