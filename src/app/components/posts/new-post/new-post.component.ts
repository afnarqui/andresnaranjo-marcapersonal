import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { PostI } from '../../../shared/models/post.interface';
import { PostService } from '../post.service';
// import { CKEditorComponent } from 'ng2-ckeditor';
// import { CKEditorComponent } from 'ng2-ckeditor';
import { AngularEditorConfig } from '@kolkov/angular-editor';
import { CursoI } from 'src/app/shared/models/curso.interface';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-new-post',
  templateUrl: './new-post.component.html',
  styleUrls: ['./new-post.component.scss']
})
export class NewPostComponent implements OnInit {
  private image: any;
  private imagenCurso: any;
  ckeditorContent: string = "<b>contenido</b>"
  htmlContent = '';

  config: AngularEditorConfig = {
    editable: true,
    spellcheck: true,
    height: '15rem',
    minHeight: '5rem',
    placeholder: 'Enter text here...',
    translate: 'no',
    defaultParagraphSeparator: 'p',
    defaultFontName: 'Arial',
    toolbarHiddenButtons: [
      ['bold']
      ],
    customClasses: [
      {
        name: "quote",
        class: "quote",
      },
      {
        name: 'redText',
        class: 'redText'
      },
      {
        name: "titleText",
        class: "titleText",
        tag: "h1",
      },
    ]
  };
  public curso$: Observable<CursoI[]>;
  cursosId;
  listaCursos = [];
  constructor(private postSvc: PostService) { }

  onContainerClic() {

  }
  public newPostForm = new FormGroup({
    titlePost: new FormControl('', Validators.required),
    contentPost: new FormControl('', Validators.required),
    //  tagsPost: new FormControl('', Validators.required),
    imagePost: new FormControl('', Validators.required),
  });

  public nuevoCursoForm = new FormGroup({
    nombre: new FormControl('', Validators.required),
    imagenCurso: new FormControl('', Validators.required),
  });

  ngOnInit() {
    this.curso$ = this.postSvc.buscarCursos();
    this.curso$.subscribe((items)=> {
      this.initValuesForm(items);
    });
  }
  initValuesForm(items): void {
    this.listaCursos = items;
    // this.newPostForm.patchValue({
    //   curso: items,
    // });
  }
  // buscarPrecios(item: any ) {
  //   this.itemsCollectionsPrices.valueChanges().subscribe((data) => {
  //    // tslint:disable-next-line: prefer-for-of
  //     for (let i = 0 ; i < data.length; i++) {
  //      this.listaPrecios.push({
  //        id: data[i].id,
  //        valor: data[i].valor,
  //        nombre: data[i].nombre
  //      });
  //    }
  //  });
  // }

  buscarCursoId(item: any, id: any ) {
    const cursosId = this.listaCursos[id];
    const data = JSON.stringify(cursosId);
    this.cursosId = cursosId.id;
  }

  addNewPost(data: PostI) {
    console.log('New post', data);
    data['contentPost'] = this.htmlContent
    data.cursosId = this.cursosId;
    this.postSvc.preAddAndUpdatePost(data, this.image);
  }

  guardarCurso(curso: CursoI) {
    const cursoObj: CursoI = {
      nombre: curso.nombre,
      imagen: '',
      fileRef: '',
      fecha: new Date(),
    };
    console.log(cursoObj);
    this.postSvc.preAddAndUpdateCurso(cursoObj, this.imagenCurso);
  }

  handleImage(event: any): void {
    this.image = event.target.files[0];
  }
  handleImagenCurso(event: any): void {
    this.imagenCurso = event.target.files[0];
  }
onChange(event: any){

}
onEditorChange(event: any){

}
onReady(event: any){

}
onFocus(event: any){

}
onBlur(event: any){

}
onContentDom(event: any){

}
onFileUploadRequest(event: any){

}
onFileUploadResponse(event: any){

}
onPaste(event: any){

}
onDrop(event: any){

}
}
