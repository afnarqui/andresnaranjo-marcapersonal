import { PostComponent } from './../../posts/post/post.component';

import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home.component';
import { MaterialModule } from '../../../material.module';
import { PostModule } from '../../posts/post/post.module';

@NgModule({
  declarations: [HomeComponent,PostComponent],
  imports: [CommonModule, HomeRoutingModule, MaterialModule,
    PostModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class HomeModule { }
