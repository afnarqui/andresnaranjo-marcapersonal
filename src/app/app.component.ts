// import { CKEditorComponent } from 'ng2-ckeditor';

import { Component, ViewChild } from '@angular/core';
import { AngularEditorConfig } from '@kolkov/angular-editor';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = '';
  htmlContent = '';

  config: AngularEditorConfig = {
    editable: true,
    spellcheck: true,
    height: '15rem',
    minHeight: '5rem',
    placeholder: 'Enter text here...',
    translate: 'no',
    defaultParagraphSeparator: 'p',
    defaultFontName: 'Arial',
    toolbarHiddenButtons: [
      ['bold']
      ],
    customClasses: [
      {
        name: "quote",
        class: "quote",
      },
      {
        name: 'redText',
        class: 'redText'
      },
      {
        name: "titleText",
        class: "titleText",
        tag: "h1",
      },
    ]
  };
  // @ViewChild(CKEditorComponent) ckEditor: CKEditorComponent;

  // ngAfterViewChecked() {
  //   let editor = this.ckEditor.instance;
  //   editor.config.height = '400';
  //   editor.config.toolbarGroups = [
  //     { name: 'document', groups: [ 'mode', 'document', 'doctools']}
  //   ];

  //   editor.config.removebuttons = 'Source,Save,Templates, Find,Replace,SelectAll, Form, Radio';
  // }

}
