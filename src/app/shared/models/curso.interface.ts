export interface CursoI {
    nombre: string;
    imagen?: any;
    id?: string;
    fileRef?: string;
    fecha?: Date;
  }