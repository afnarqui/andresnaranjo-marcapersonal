// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    // apiKey: "AIzaSyBj4h3u8zR16TgyBSvKx1j-Ycdj3OjUZcE",
    // authDomain: "rutinacalestenia.firebaseapp.com",
    // projectId: "rutinacalestenia",
    // storageBucket: "rutinacalestenia.appspot.com",
    // messagingSenderId: "1035122184030",
    // appId: "1:1035122184030:web:66f321fd6e2f36f3e049cf"
    apiKey: "AIzaSyDG-2BS1MQNpmDDIf979jPomf9rDoIiV78",
    authDomain: "joncalestenia.firebaseapp.com",
    projectId: "joncalestenia",
    storageBucket: "joncalestenia.appspot.com",
    messagingSenderId: "746037724032",
    appId: "1:746037724032:web:726e91220f7be89722f765"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
